####Status   
  #Backend (convert proxy) - Done   
  #Frontend - Done   
  #Docker - Done   
  
  Git start  
  
  ```
  npm run install:all
  ```  
  ```
  npm run start:build  
  ```
  
  Docker build  
  ```
  docker build -t purpleconverter .  
  ```  
  ```
  docker run -p 3000:3000 purpleconverter  
  ```  
  Docker start  
  ```
  sudo docker run -p 3000:3000 sangowen/purpleconverter:1.0.1
  ```  