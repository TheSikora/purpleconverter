const path = require('path');

var src = path.resolve(__dirname, 'src');
var control = path.resolve(__dirname, 'src/controls');

var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });

module.exports = {
  entry: "./src/index.ts",
  output: {
      filename: "index.js",
      path: __dirname + "/../build/"
  },
  devtool: "source-map",
  target: 'node',
  resolve: {
      extensions: [".ts", ".tsx", ".js", ".json"],
      alias: {
        'src': src,
        'controls': control
      }
  },
  externals: nodeModules,
  module: {
      rules: [
          { test: /\.tsx?$/, loader: "ts-loader" },
      ]
  }
};
