import * as path from 'path';
import * as http from 'http';
import * as express from 'express';
import * as graphqlHTTP from 'express-graphql';
import { buildSchema } from 'graphql';
import * as fs from 'fs';
import { GraphQlControl } from 'controls/GraphQlControl';

export type ServerInfo = {
  address: string;
  family: string;
  port: number;
};

export class ServerControl {
  private app: express.Express;
  private server: http.Server;
  private serverInfo: ServerInfo;
  private graphQlControl: GraphQlControl;

  constructor() {
    this.initServer();

    this.loadStaticFiles();
  }

  public startListening(): Promise<ServerInfo> {
    return new Promise<ServerInfo>((resolve: (info: ServerInfo) => void): void => {
      this.server = this.app.listen(3000, '0.0.0.0', () => {
        resolve(this.server.address());
      });
    });
  }

  public stopListening(): Promise<void> {
    return new Promise<void>((resolve: () => void): void => {
      this.server.close(resolve);
    });
  }

  private initServer(): void {
    this.app = express();
    this.graphQlControl = new GraphQlControl();
  }

  private loadStaticFiles(): void {
    this.app.use('/graphql', this.graphQlControl.getGrapqlHTTP());
    this.app.use('/', express.static('./static'));
  }
}
