import { StorageControl } from 'controls/StorageControl';
import { ApiControl, Currencies } from 'controls/ApiControl';
import * as graphqlHTTP from 'express-graphql';
import { buildSchema } from 'graphql';

export type Resolvers = {
  convert: (currency: string, toCurrency: string, amount: number) => number;
  currencies: () => string[];
  popularDesc: () => string;
  convertionAmount: () => number;
  convertionValue: () => number;
};

export class GraphQlControl {
  private storeControl: StorageControl;
  private apiControl: ApiControl;

  constructor() {
    if (process.env.NODE_ENV === 'dev') {
      this.storeControl = new StorageControl();
    } else {
      this.storeControl = new StorageControl('/tmp/prod_storage');
    }
    this.apiControl = new ApiControl(this.storeControl);
  }

  public getGrapqlHTTP(): graphqlHTTP.Middleware {
    return graphqlHTTP({
      schema: buildSchema(SCHEMA),
      rootValue: this.getResolvers(),
      graphiql: process.env.NODE_ENV === 'dev'
    });
  }

  public convert({currency, toCurrency, amount}: {currency: string; toCurrency: string; amount: number}): Promise<number> {
    return this.apiControl.convert(currency, toCurrency, amount).then((result: number) => {
      if (amount === 0) {
        return 0;
      }
      if (!result) {
        throw new Error('Currency unknown');
      }

      if (amount < 0) {
        throw new Error('Amount must be above 0');
      }

      return result;
    });
  }

  public getAllCurrencies(): Promise<string[]> {
    return this.apiControl.getAll().then((items: Currencies) => {
      return Object.keys(items);
    });
  }

  public getPopularDecs(): string {
    return this.storeControl.getMostPopularDest();
  }

  public getConvertionAmount(): number {
    return this.storeControl.getAmount();
  }

  public getConvertionValue(): number {
    return this.storeControl.getValue();
  }

  private getResolvers(): Resolvers {
    return {
      convert: this.convert.bind(this),
      currencies: this.getAllCurrencies.bind(this),
      popularDesc: this.getPopularDecs.bind(this),
      convertionAmount: this.getConvertionAmount.bind(this),
      convertionValue: this.getConvertionValue.bind(this)
    };
  }
}

const SCHEMA: string = `
type Query {
  convert(currency: String, toCurrency: String, amount: Float): Float,
  currencies: [String],
  popularDesc: String
  convertionAmount: Int,
  convertionValue: Float,
}
`;
