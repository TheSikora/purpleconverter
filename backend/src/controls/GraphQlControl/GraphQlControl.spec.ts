import { GraphQlControl } from 'controls/GraphQlControl';
import { Currencies } from 'controls/ApiControl';
import { StorageControl } from 'controls/StorageControl';

describe('GraphQlControl', () => {
  it('Initialize GraphQlControl class, should not crash', () => {
    new GraphQlControl(); // tslint:disable-line
  });

  describe('Resolvers', () => {
    let graphQlControl: GraphQlControl;

    beforeEach(() => {
      new StorageControl().cleanUp();
      graphQlControl = new GraphQlControl();
    });

    it('Get all currencies', () => {
      expect.assertions(1);

      return graphQlControl.getAllCurrencies().then((result: string[]) => {
        expect(result.indexOf('USD')).not.toBe(-1);
      });
    });

    it('Convert', () => {
      expect.assertions(1);
      const currency: string = 'USD';
      const amount: number = 1;

      return graphQlControl.convert({ currency, toCurrency: currency, amount }).then((result: number) => {
        expect(result).toBe(amount);
      });
    });

    it('Convert non existing', () => {
      expect.assertions(1);
      const currency: string = 'YOLO';
      const amount: number = 1;

      return graphQlControl.convert({ currency, toCurrency: currency, amount }).catch((error: Error) => {
        expect(typeof error.message).toBe('string');
      });
    });

    it('Most popular destination', () => {
      expect.assertions(1);
      const currency: string = 'USD';
      const amount: number = 1;

      return graphQlControl.convert({ currency, toCurrency: currency, amount }).then((result: number) => {
        expect(graphQlControl.getPopularDecs()).toBe('USD');
      });
    });

    it('Most popular destination, with non', () => {
      expect(graphQlControl.getPopularDecs()).toBe('');
    });

    it('Most value, with non', () => {
      expect(graphQlControl.getConvertionAmount()).toBe(0);
    });

    it('Amount', () => {
      expect.assertions(1);
      const currency: string = 'USD';
      const amount: number = 1;

      return graphQlControl.convert({ currency, toCurrency: currency, amount }).then((result: number) => {
        expect(graphQlControl.getConvertionAmount()).toBe(1);
      });
    });

    it('Value', () => {
      expect.assertions(1);
      const currency: string = 'USD';
      const amount: number = 1;

      return graphQlControl.convert({ currency, toCurrency: currency, amount }).then((result: number) => {
        expect(graphQlControl.getConvertionValue()).toBe(amount);
      });
    });

    it('Most amount, with non', () => {
      expect(graphQlControl.getConvertionValue()).toBe(0);
    });
  });
});
