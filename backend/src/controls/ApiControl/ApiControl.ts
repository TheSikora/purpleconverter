import axios, { AxiosResponse } from 'axios';
import { StorageControl } from 'controls/StorageControl';

const BASE_URL: string = 'https://openexchangerates.org/api';
const ALL_CURRENCIES: string = '/currencies.json';
const APP_ID: string = 'd05d7ac48f9043a58dc10d9bdc803a9b';
const ALL_LATEST: string = `/latest.json?app_id=${APP_ID}`;
const BASE: string = 'USD';

export type Currencies = {
  [curr in string]: string;
};

export class ApiControl {
  private storeControl?: StorageControl;

  constructor(storeControl?: StorageControl) {
    this.storeControl = storeControl;
  }

  public getAll(): Promise<Currencies> {
    return axios({
      method: 'get',
      baseURL: BASE_URL,
      url: ALL_CURRENCIES
    }).then((response: AxiosResponse) => {
      return response.data;
    });
  }

  public convert(currency: string, toCurrency: string, amount: number): Promise<number | null> {
    return axios({
      method: 'get',
      baseURL: BASE_URL,
      url: ALL_LATEST
    }).then((response: AxiosResponse) => {
      if (!response.data.rates[toCurrency] || !response.data.rates[currency]) {
        return null;
      }
      const usdAmount: number = (currency === BASE) ? amount : amount / response.data.rates[currency];
      if (this.storeControl) {
        this.storeControl.addConversion(currency, toCurrency, usdAmount);
      }

      return usdAmount * response.data.rates[toCurrency];
    });
  }
}
