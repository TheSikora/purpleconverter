import { ApiControl, Currencies } from 'controls/ApiControl';
import { StorageControl } from 'controls/StorageControl';
import * as moxios from 'moxios';

describe('ApiControl', () => {
  it('Initialize File class, should not crash', () => {
    new ApiControl(); // tslint:disable-line
  });

  describe('Calls', () => {
    let apiControl: ApiControl;

    afterEach(() => {
      moxios.uninstall();
    });

    beforeEach(() => {
      apiControl = new ApiControl();
      moxios.install();
      moxios.wait(() => {
        const request: any = moxios.requests.mostRecent(); // tslint:disable-line
        request.respondWith({
          status: 200,
          response: {
            rates: {
              CZK: 25,
              USD: 1
            },
            USD: 'United States Dollar'
          }
        });
      });
    });

    it('Get all currencies', () => {
      expect.assertions(1);

      return apiControl.getAll().then((items: Currencies) => {
        expect(typeof items).not.toBe('undefined');
      });
    });

    it('Get all currencies, check for USD', () => {
      expect.assertions(1);

      return apiControl.getAll().then((items: Currencies) => {
        expect(items.USD).toBe('United States Dollar');
      });
    });

    it('Convert', () => {
      expect.assertions(1);

      return apiControl.convert('USD', 'CZK', 1000).then((amount: number) => {
        expect(typeof amount).not.toBe('undefined');
      });
    });

    it('Convert with value check', () => {
      expect.assertions(1);

      return apiControl.convert('USD', 'CZK', 1).then((amount: number) => {
        expect(amount).toBe(25);
      });
    });

    it('Convert with value check, base not USD', () => {
      expect.assertions(1);

      return apiControl.convert('CZK', 'USD', 100).then((amount: number) => {
        expect(amount).toBe(4);
      });
    });

    it('Convert to Non existing', () => {
      expect.assertions(1);

      return apiControl.convert('CZK', 'YOLO', 100).then((amount: number) => {
        expect(amount).toBe(null);
      });
    });

    it('Convert from Non existing', () => {
      expect.assertions(1);

      return apiControl.convert('YOLO', 'CZK', 100).then((amount: number) => {
        expect(amount).toBe(null);
      });
    });
  });
  describe('Store', () => {
    let apiControl: ApiControl;
    let storageControl: StorageControl;

    beforeEach(() => {
      storageControl = new StorageControl();
      storageControl.cleanUp();
      apiControl = new ApiControl(storageControl);
      moxios.install();
      moxios.wait(() => {
        const request: any = moxios.requests.mostRecent(); // tslint:disable-line
        request.respondWith({
          status: 200,
          response: {
            rates: {
              CZK: 25,
              USD: 1
            },
            USD: 'United States Dollar'
          }
        });
      });
    });

    afterEach(() => {
      moxios.uninstall();
    });

    it('Convert', () => {
      expect.assertions(3);
      const toCurrency: string = 'CZK';
      const amount: number = 1000;

      return apiControl.convert('USD', toCurrency, amount).then(() => {
        expect(storageControl.getMostPopularDest()).toBe(toCurrency);
        expect(storageControl.getAmount()).toBe(1);
        expect(storageControl.getValue()).toBe(amount);
      });
    });
  });
});
