import { Storage } from 'controls/StorageControl/Storage';

enum Storage_keys {
  dest = 'destinations',
  amout = 'amount',
  value = 'value'
}

export type Destinations = {
  [dest in string]: number;
};

export class StorageControl {
  private store: Storage;
  private destinations: Destinations;
  private amount: number;
  private value: number;

  constructor(storePath?: string) {
    this.store = new Storage(storePath);
    this.destinations = JSON.parse(this.store.load(Storage_keys.dest, '{}'));
    this.amount = parseInt(this.store.load(Storage_keys.amout, '0'), 10);
    this.value = parseInt(this.store.load(Storage_keys.value, '0'), 10);
  }

  public addConversion(fromCur: string, to: string, amount: number): void {
    this.saveDestination(to);
    this.incAmount();
    this.incValue(amount);
  }

  public cleanUp(): void {
    this.destinations = {};
    this.amount = 0;
    this.value = 0;
    this.store.cleanStore();
  }

  public getMostPopularDest(): string {
    let max: string = '';
    const keys: string[] = Object.keys(this.destinations);
    for (const key of keys) {
      const value: number = this.destinations[key];
      if (max === '' || value > this.destinations[max]) {
        max = key;
      }
    }

    return max;
  }

  public getAmount(): number {
    return this.amount;
  }

  public getValue(): number {
    return this.value;
  }

  private incValue(value: number): void {
    this.value += value;
    this.store.save(Storage_keys.value, `${this.value}`);
  }

  private incAmount(): void {
    this.amount += 1;
    this.store.save(Storage_keys.amout, `${this.amount}`);
  }

  private saveDestination(to: string): void {
    const destAmount: number = this.destinations[to] || 0;
    this.destinations[to] = destAmount + 1;
    this.store.save(Storage_keys.dest, JSON.stringify(this.destinations));
  }
}
