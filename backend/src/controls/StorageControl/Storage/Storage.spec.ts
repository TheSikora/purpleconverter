import { Storage } from 'controls//StorageControl/Storage';

describe('Storage', () => {
  it('Initialize File class, should not crash', () => {
    new Storage(); // tslint:disable-line
  });

  describe('Save/Recover', () => {
    let storage: Storage;

    beforeEach(() => {
      storage = new Storage();
    });

    it('Store item, should not crash', () => {
      storage.save('key', 'randomString');
    });

    it('Recoved item, should not crash', () => {
      storage.load('key');
    });

    it('Recoved item, not stored key', () => {
      storage.cleanStore();
      expect(storage.load('key')).toBe('');
    });

    it('Recoved item, not stored key with default', () => {
      const test: string = 'default';
      storage.cleanStore();
      expect(storage.load('key', test)).toBe(test);
    });

    it('Recoved stored item', () => {
      const key: string = 'key2';
      const item: string = 'item';
      storage.save(key, item);

      expect(storage.load(key)).toBe(item);
    });

    it('Recoved stored item from another storage instance', () => {
      const key: string = 'key3';
      const item: string = 'item';
      storage.save(key, item);
      storage = new Storage();

      expect(storage.load(key)).toBe(item);
    });
  });
});
