import * as fs from 'fs';

const STORAGE: string = '/tmp/storage_file';

export class Storage {
  private store: any;// tslint:disable-line
  private storePath: string;

  constructor(storePath: string = STORAGE) {
    this.storePath = storePath;
    if (fs.existsSync(this.storePath)) {
      this.store = JSON.parse(fs.readFileSync(this.storePath).toString());
    } else {
      this.store = {};
    }
  }

  public cleanStore(): void {
    if (fs.existsSync(this.storePath)) {
      fs.unlinkSync(this.storePath);
    }
    this.store = {};
  }

  public save(key: string, item: string): void {
    this.store[key] = item;
    this.writeStore();
  }

  public load(key: string, defaultValue: any = ''): string { // tslint:disable-line
    return this.store[key] || defaultValue;
  }

  private writeStore(): void {
    fs.writeFileSync(this.storePath, JSON.stringify(this.store));
  }
}
