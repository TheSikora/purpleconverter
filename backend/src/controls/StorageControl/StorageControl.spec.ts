import { StorageControl } from 'controls/StorageControl';

describe('StorageControl', () => {
  it('Initialize StorageControl class, should not crash', () => {
    new StorageControl(); // tslint:disable-line
  });

  describe('Add conversion', () => {
    let storageControl: StorageControl;

    beforeEach(() => {
      storageControl = new StorageControl();
    });

    it('Add convesion, should not crash', () => {
      storageControl.addConversion('USD', 'CZK', 100);
    });

    it('Clean up, should not crash', () => {
      storageControl.cleanUp();
    });
  });

  describe('Get most popular destination', () => {
    let storageControl: StorageControl;

    beforeEach(() => {
      storageControl = new StorageControl();
      storageControl.cleanUp();
    });

    it('init, should not crash', () => {
      storageControl.getMostPopularDest();
    });

    it('Empty storage', () => {
      expect(storageControl.getMostPopularDest()).toBe('');
    });

    it('One dest', () => {
      const dest: string = 'CZK';
      storageControl.addConversion('USD', dest, 100);
      expect(storageControl.getMostPopularDest()).toBe(dest);
    });

    it('More dest', () => {
      const dest: string = 'CZK';
      storageControl.addConversion('USD', dest, 100);
      storageControl.addConversion('USD', dest, 100);
      storageControl.addConversion('USD', 'SOME', 100);
      expect(storageControl.getMostPopularDest()).toBe(dest);
    });

    it('More dest, from another instance', () => {
      const dest: string = 'CZK';
      storageControl.addConversion('USD', dest, 100);
      storageControl.addConversion('USD', dest, 100);
      storageControl.addConversion('USD', 'SOME', 100);
      const storageControl2: StorageControl = new StorageControl();
      expect(storageControl2.getMostPopularDest()).toBe(dest);
    });
  });

  describe('Get amount of conversions', () => {
    let storageControl: StorageControl;

    beforeEach(() => {
      storageControl = new StorageControl();
      storageControl.cleanUp();
    });

    it('init, should not crash', () => {
      storageControl.getAmount();
    });

    it('Empty storage', () => {
      expect(storageControl.getAmount()).toBe(0);
    });

    it('With conversions', () => {
      const amount: number = 100;
      for (let i: number = 0; i < amount; i += 1) {
        storageControl.addConversion('USD', 'SOME', 100);
      }
      expect(storageControl.getAmount()).toBe(amount);
    });

    it('With conversions, another instance', () => {
      const amount: number = 100;
      for (let i: number = 0; i < amount; i += 1) {
        storageControl.addConversion('USD', 'SOME', 100);
      }
      const storageControl2: StorageControl = new StorageControl();
      expect(storageControl2.getAmount()).toBe(amount);
    });
  });

  describe('Get total value', () => {
    let storageControl: StorageControl;

    beforeEach(() => {
      storageControl = new StorageControl();
      storageControl.cleanUp();
    });

    it('init, should not crash', () => {
      storageControl.getValue();
    });

    it('Empty storage', () => {
      expect(storageControl.getValue()).toBe(0);
    });

    it('With conversions', () => {
      const amount: number = 100;
      const value: number = 100;
      for (let i: number = 0; i < amount; i += 1) {
        storageControl.addConversion('USD', 'SOME', value);
      }
      expect(storageControl.getValue()).toBe(amount * value);
    });

    it('With conversions, another instance', () => {
      const amount: number = 100;
      const value: number = 100;
      for (let i: number = 0; i < amount; i += 1) {
        storageControl.addConversion('USD', 'SOME', value);
      }
      const storageControl2: StorageControl = new StorageControl();
      expect(storageControl2.getValue()).toBe(amount * value);
    });
  });
});
