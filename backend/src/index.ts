import { ServerControl, ServerInfo } from 'controls/ServerControl';

const server: ServerControl = new ServerControl();
server.startListening().then(({ address, port }: ServerInfo): void => {
  // tslint:disable-next-line
  console.log(`Server listening at address ${address} on port ${port}`);
});
