import * as React from "react";
import * as ReactDOM from "react-dom";
import { MainScene } from 'scenes/MainScene';

ReactDOM.render(
    <MainScene />,
    document.getElementById("root")
);