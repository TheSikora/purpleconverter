import * as React from 'react';

import './style.scss';

type Props = {
  id?: string,
  submitCallback: (input: string, choice: string, choiceTo: string) => void,
  choices: string[]
}

type State = {
  input: string,
  choice: string,
  choiceTo: string,
}

export class InputPanel extends React.Component<Props, State> {
  private handleChoice: (event: any) => void;
  private handleChoiceTo: (event: any) => void;
  private handleInput: (event: any) => void;
  private handleSubmit: (event: any) => void;

  constructor(props: Props) {
    super(props);
    this.state = {
      input: '0',
      choice: '-',
      choiceTo: '-'
    };

    this.handleChoice = this.handleChoiceChange.bind(this);
    this.handleChoiceTo = this.handleChoiceToChange.bind(this);
    this.handleInput = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleInputSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps: Props) {
    const { choices } = this.props;
    if (nextProps.choices !== choices) {
      this.setState({
        choice: nextProps.choices[0],
        choiceTo: nextProps.choices[0]
      });
    }
  }

  handleChoiceChange(event: any) {
    this.setState({ choice: event.target.value });
  }

  handleChoiceToChange(event: any) {
    this.setState({ choiceTo: event.target.value });
  }

  handleInputChange(event: any) {
    const value: number = event.target.value;
    if (value >= 0) {
      this.setState({ input: event.target.value });
    }
  }

  handleInputSubmit(event: any) {
    event.preventDefault();
    const { submitCallback } = this.props;
    const { input, choice, choiceTo } = this.state;
    submitCallback(input, choice, choiceTo);
  }

  render() {
    const { handleInput, handleChoice } = this;
    const { submitCallback, choices } = this.props;
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          <input className="inputPanel" value={this.state.input} type="number" step="0.1" onChange={this.handleInput}/>
          <div>
            <select className="inputPicker" value={this.state.choice} onChange={this.handleChoice}>
              { choices.map((item) => {
                return <option key={item} value={item}>{item}</option>
              }) }
            </select>
          </div>
          <div>
            <select className="inputPicker" value={this.state.choiceTo} onChange={this.handleChoiceTo}>
              { choices.map((item) => {
                return <option key={item} value={item}>{item}</option>
              }) }
            </select>
          </div>
        </label>
        <div>
          <input className="inputSubmit" type="submit" value="Submit" />
        </div>
      </form>
    );
  }
}
