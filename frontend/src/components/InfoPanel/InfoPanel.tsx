import * as React from 'react';

import './style.scss';

type Props = {
  text?: string,
  id?: string,
}

export class InfoPanel extends React.Component<Props> {

  static defaultProps: Props = {
    text: 'None',
    id: 'defaultID'
  }

  render() {
    const { id, text } = this.props;
    return <div id={id} className='infoPanel' >{text}</div>
  }
}
