export const popularDesc: string = `
  query {
    result: popularDesc,
  }
`.replace(/\s/g,'');

export const convertionValue: string = `
  query {
    result: convertionValue,
  }
`.replace(/\s/g,'');

export const convertionAmount: string = `
  query {
    result: convertionAmount
  }
`.replace(/\s/g,'');

export const currencies: string = `
  query {
    result: currencies
  }
`.replace(/\s/g,'');

export const getConvert = (currency: string, toCurrency: string, amount: string): string => `
  query {
    result: convert(currency: "${currency}", toCurrency: "${toCurrency}", amount: ${amount})
  }
`.replace(/\s/g,'');
