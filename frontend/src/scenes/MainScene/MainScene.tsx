import * as React from 'react';
import { InfoPanel } from 'components/InfoPanel';
import { InputPanel } from 'components/InputPanel';
import { ApiControl } from 'controls/ApiControl';

import './style.scss';

type Props = {

}

type State = {
  choices: string[];
  result: string;
  popular: string;
  value: string;
  amount: string;
}

export class MainScene extends React.Component<Props, State> {
  private apiControl: ApiControl;
  private handleSubmit: (input: string, choice: string, choiceTo: string) => void;

  constructor(props: Props) {
    super(props);
    this.state = {
      choices: [],
      result: '-',
      popular: '-',
      value: '-',
      amount: '-',
    }
    this.apiControl = new ApiControl();
    this.handleSubmit = this.handleFormSubmit.bind(this);
    this.loadCurrencies();
    this.loadStats();
  }

  private loadStats(): void {
    this.loadAmount();
    this.loadPopular();
    this.loadValue();
  }

  private loadCurrencies(): void {
    this.apiControl.getCurrencies().then((result: any) => {
      this.setState({ choices: result });
    });
  }

  private loadPopular() {
    this.apiControl.getPopularDestination().then((result: any) => {
      this.setState({ popular: result });
    });
  }

  private loadValue() {
    this.apiControl.getConversionValue().then((result: any) => {
      this.setState({ value: result });
    });
  }

  private loadAmount() {
    this.apiControl.getConversionAmount().then((result: any) => {
      this.setState({ amount: result });
    });
  }

  private handleFormSubmit(input: string, choice: string, choiceTo: string): void {
    this.apiControl.getConversionResult(input, choice, choiceTo).then((result: any) => {
      if (result === null) {}
      this.setState({ result: `${(result === null) ? -1 : result}` });
    }).then(() => {
      this.loadStats();
    });
  }

  public render() {
    const { choices, result, popular, value, amount } = this.state;
    return (
      <>
        <div className="infoWrap">
          <InfoPanel id='value' text={value}/>
          <InfoPanel id='pop' text={popular} />
          <InfoPanel id='amount' text={amount} />
        </div>
        <div className="inputWrap">
          <InputPanel submitCallback={this.handleSubmit} choices={choices} />
        </div>
        <div className="resultWrap">
          <InfoPanel id='result' text={result} />
        </div>
      </>
    );
  }
}
