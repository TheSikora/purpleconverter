import {
  popularDesc,
  convertionAmount,
  convertionValue,
  currencies,
  getConvert
} from 'utils/graphql';
import axios, { AxiosResponse } from 'axios';

export class ApiControl {

  getPopularDestination(): Promise<string> {
    return this.get(popularDesc).then((response: AxiosResponse) => {
      return response.data.data.result;
    });
  }

  getConversionAmount(): Promise<number> {
    return this.get(convertionAmount).then((response: AxiosResponse) => {
      return response.data.data.result;
    });
  }

  getConversionValue(): Promise<number> {
    return this.get(convertionValue).then((response: AxiosResponse) => {
      return response.data.data.result;
    });
  }

  getCurrencies(): Promise<string[]> {
    return this.get(currencies).then((response: AxiosResponse) => {
      return response.data.data.result;
    });
  }

  getConversionResult(input: string, choice: string, choiceTo: string): Promise<string[]> {
    return this.get(getConvert(choice, choiceTo, input)).then((response: AxiosResponse) => {
      return response.data.data.result;
    });
  }

  get(data: string) {
    return axios({
      method: 'get',
      url: `/graphql?query=${data}`,
    });
  }
}