import { ApiControl } from 'controls/ApiControl';
import * as moxios from 'moxios';

describe('ApiControl', () => {
  let apiControl: ApiControl;

  beforeEach(() => {
    apiControl = new ApiControl();
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  it('Get all currencies', () => {
    expect.assertions(1);

    const result: string[] = ['ZSS'];

    moxios.wait(() => {
      const request: any = moxios.requests.mostRecent(); // tslint:disable-line
      request.respondWith({
        status: 200,
        response: {
          data: {
            result,
          },
        }
      });
    });

    return apiControl.getCurrencies().then((items: any) => {
      expect(items).toBe(result);
    });
  });

  it('getPopularDestination', () => {
    expect.assertions(1);

    const result: string = 'ZSS';

    moxios.wait(() => {
      const request: any = moxios.requests.mostRecent(); // tslint:disable-line
      request.respondWith({
        status: 200,
        response: {
          data: {
            result,
          },
        }
      });
    });

    return apiControl.getPopularDestination().then((items: any) => {
      expect(items).toBe(result);
    });
  });

  it('getConversionAmount', () => {
    expect.assertions(1);

    const result: number = 5;

    moxios.wait(() => {
      const request: any = moxios.requests.mostRecent(); // tslint:disable-line
      request.respondWith({
        status: 200,
        response: {
          data: {
            result,
          },
        }
      });
    });

    return apiControl.getConversionAmount().then((items: any) => {
      expect(items).toBe(result);
    });
  });

  it('getConversionValue', () => {
    expect.assertions(1);

    const result: number = 5;

    moxios.wait(() => {
      const request: any = moxios.requests.mostRecent(); // tslint:disable-line
      request.respondWith({
        status: 200,
        response: {
          data: {
            result,
          },
        }
      });
    });

    return apiControl.getConversionValue().then((items: any) => {
      expect(items).toBe(result);
    });
  });

  it('getConversionResult', () => {
    expect.assertions(1);

    const result: number = 5;

    moxios.wait(() => {
      const request: any = moxios.requests.mostRecent(); // tslint:disable-line
      request.respondWith({
        status: 200,
        response: {
          data: {
            result,
          },
        }
      });
    });

    return apiControl.getConversionResult('3', 'fds', '3fds').then((items: any) => {
      expect(items).toBe(result);
    });
  });
});
