const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

var components = path.resolve(__dirname, 'src/components');
var scenes = path.resolve(__dirname, 'src/scenes');
var utils = path.resolve(__dirname, 'src/utils');
var controls = path.resolve(__dirname, 'src/controls');

module.exports = {
  entry: "./src/index.tsx",
  output: {
      filename: "bundle.js",
      path: __dirname + "/../build/static"
  },
  devtool: "source-map",
  resolve: {
      extensions: [".ts", ".tsx", ".js", ".json"],
      alias: {
        'components': components,
        'scenes': scenes,
        'utils': utils,
        'controls': controls,
      }
  },
  plugins: [
    new HtmlWebpackPlugin({
        title: 'Purple converter',
        template: './src/index.html'
    })
  ],
  module: {
      rules: [
          { test: /\.tsx?$/, loader: "ts-loader" },
          { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
          { test: /\.scss$/, use: [{
            loader: "style-loader" // creates style nodes from JS strings
        }, {
            loader: "css-loader" // translates CSS into CommonJS
        }, {
            loader: "sass-loader" // compiles Sass to CSS
        }] }
      ]
  }
};
