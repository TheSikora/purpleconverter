FROM node:9.10.0

EXPOSE 3000

COPY . .
RUN npm run install:all
RUN npm run build

CMD ["npm", "run", "start"]
